/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3_unittest;

/**
 *
 * @author USER
 */
class OXProgram {

    static boolean checkWin(char[][] table, char currentPlayer) {
        if (checkRow(table, currentPlayer) || checkCol(table, currentPlayer) || checkFirstDiagnol(table, currentPlayer) || checkSecondDiagnol(table, currentPlayer) || checkDraw(table, currentPlayer)) {
            return true;
        }

        return false;

    }

    private static boolean checkRow(char[][] table, char currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == currentPlayer && table[row][1] == currentPlayer && table[row][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] table, char currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkFirstDiagnol(char[][] table, char currentPlayer) {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkSecondDiagnol(char[][] table, char currentPlayer) {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw(char[][] table, char currentPlayer) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] != currentPlayer && table[row][col] == '-') {
                    return false;
                }
            }

        }
        return true;
    }

}
