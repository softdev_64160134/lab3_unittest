/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3_unittest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author USER
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testCheckWinnerNoPlayBY_X_output_false() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRowBy_O_output_true() {
        char[][] table = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinColBy_X_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'X', 'O', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2By_O_output_false() {
        char[][] table = {{'-', 'O', 'X'}, {'-', 'O', '-'}, {'X', 'O', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinFirstDignolBy_X_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'O', 'X', '-'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinSecondDignolBy_O_output_true() {
        char[][] table = {{'O', 'X', 'O'}, {'X', 'O', '-'}, {'O', '-', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDrawBy_X_output_true() {
        char[][] table = {{'X', 'O', 'X'}, {'O', 'X', 'O'}, {'O', 'X', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckDrawBy_X_output_false() {
        char[][] table = {{'X', 'O', 'X'},{'O', 'X', 'O'},{'O', 'X', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckIncompleteBy_X_output() {
        char[][] table = {{'X', '-', '-'},{'-', 'X', '-'},{'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

}
